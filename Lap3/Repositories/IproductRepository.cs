﻿using Lap3.Models;

namespace Lap3.Repositories
{
    public interface IproductRepository
    {
        Task<IEnumerable<Product>> GetAllAsync();
        Task<Product> GetByIdAsync(int id);
        Task AddAsync(Product product);
        Task UpdateAsync(Product product);

        Task DeleteAsync(int id);

    }
}
