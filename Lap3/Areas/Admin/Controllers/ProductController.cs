﻿using Lap3.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Lap3.Areas.Admin.Controllers
{
    public class ProductController : Controller
    {
        [Area("Admin")]
        [Authorize(Roles =SD.Role_Admin)]
        public IActionResult Index()
        {
            return View();
        }
    }
}
